# ---------------------------------------------- Build Time Arguments --------------------------------------------------
ARG NODE_VERSION="14.18"

# ======================================================================================================================
#                                                   --- Base ---
# ---------------  This stage install needed extenstions, plugins and add all needed configurations  -------------------
# ======================================================================================================================
FROM node:${NODE_VERSION}-alpine as base
LABEL maintainer="marc.bischof@swisscom.com"

WORKDIR /app
COPY package.json .
COPY package-lock.json* .

# ------------------------------------------------- Dependencies -------------------------------------------------------
FROM base AS dependencies
RUN npm set progress=false && npm config set depth 0
RUN npm ci

# ----------------------------------------------------- Build ----------------------------------------------------------
FROM base AS build
COPY --from=dependencies /app/node_modules ./node_modules
COPY pages ./pages
COPY public ./public
COPY styles ./styles
COPY tsconfig.json ./tsconfig.json
COPY next.config.js ./next.config.js
RUN npm run build

# ======================================================================================================================
# ==============================================  PRODUCTION IMAGE  ====================================================
#                                                   --- PROD ---
# ======================================================================================================================
# --------------------------------------------------- Prod Image -------------------------------------------------------
FROM base as app
COPY --from=build /app/node_modules ./node_modules
COPY --from=build /app/next.config.js ./next.config.js
COPY --from=build /app/public ./public
COPY --from=build /app/.next ./.next

ENV PORT=3000
EXPOSE 3000
CMD ["npm", "start"]
